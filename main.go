package main

import (
	"io"
	"net/http"
)

func handler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Hi this service is updated via portainer webhook!")
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8000", nil)
}
